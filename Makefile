LDLIBS = -lcurses -lespeak
SRCS = ipage.c
CCFLAGS=-pthread
CC = gcc
OBJS = ipage
RM = /bin/rm -f

all: ipage

install:
	cp ipage /usr/local/bin

clean:
	$(RM) $(OBJS)

rebuild: cleen \
 all
