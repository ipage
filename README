***	Ipage	***
	An intelligent pager for the console.

Introduction

Ipage is a pager for the Linux console and possibly some other *nixes. It can do your standard paging functions e.g. 
page down, page up, line down, line up, search etc.
Ipage has been optimized to be comfortable for a blind user to use e.a. it presents it's output in a format easily 
understandable for a screen reader like speakup (http://linux-speakup.org).
In addition, ipage has a "say all" function. By pressing "a", the file will be read aloud from the current cursor 
position. Pressing "c" while in "say all" mode, the operation will be stopped and the cursor moved to the last line of 
text spoken.
Ipage will also automatically save the last position in the file to a file called <filename>.ipm on exit, where <filename> 
is the name of the current file. All set marks will also be saved.

Keys

The following is a list of keys that can be use to operate ipage:
down arrow:
go to the next line
up arrow:
go to the previous line
page down or space:
go to the next page
page up or b:
go to the previous page
/:
search for a string of text
>:
search for the next occurrence of the string previously searched for by /
<:
search for the previous occurrence of the string previously searched for by /
1 and 2:
lower and raise the rate of the speech respectively
v:
change the voice of the speech
s followed by a letter:
set a mark at the current position and assign it to the letter
m followed by a letter:
move to the position previously assigned to the letter
home:
go to beginning of file
end:
go to end of file
a:
start the say all function
c:
stop the say all function and update the cursor

Dependencies

Ipage will knead the following components to run on the system:
* espeak, for speech output
* fmt, to prittyprint the file before viewing
* ncurses, for output on the screen (it is possible that another version/flavor of curses will work too)

Installing

To compile and install ipage given that all dependencies are met:
$ make
$ sudo make install

To Do

This is just a list of things I might still do to ipage.
* make the search function support regular expressions
* make the say all function automatically scroll the file while reading
* support other speech engines besides espeak (speech-dispatcher?)

Contributing

This is an open source project, so contributing is most welcome. If you want to contribute in any way, please drop me an 
email at:
rynkruger@gmail.com

Copyright and License

Ipage is Distrebuted under the terms of the GNU General Public License version 3. You can view the license in the file 
License.txt. It boils down to this:

You may copy this program and give it to any one you like. You may modify the program as long as you state clearly which 
modifications are yours and give the source code to any one who wants it. 
You may not use the code in a close-source project without the permission of the copyright holder (yours truely)

Last updated by Rynhardt Kruger on March 17th, 2011

