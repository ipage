/***************************************************************************
 *   Copyright (C) 2010,2011 by Rynhardt Kruger                            *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

#include <curses.h>
#include <wchar.h>
#include <espeak/speak_lib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define _GNU_SOURCE

char filename[2000];
long linenum = 1;
char evoice[40];
char epunct[50];
long lastline = 0;
int chars = 0;
int offset = 0;
const int ratemult = 35;
int rate = 5;
int sayall = 0;
FILE * source;
char mainbuf[1024*64];
int mainlen = 1024*64;
int mainpos = 0;
int datapos = 0;
char sbuf [100];
char markname[1000];
int marks[27];

// prototypes
char prevchar(FILE * source);
int prevline(char * data);
int gotoline(int n);
int nextline(char * data);

int nextchunc() {
    int i = 0;
    if (feof(source))
        return 1;
    while (! feof(source)&& i < 1024*64) {
        int f = fgetc(source);
        if (f == EOF)
            break;
        mainbuf[i] = f;
        i++;
    }
    mainlen = i;
    mainpos = 0;
    return 0;
}

int prevchunc() {
    if (ftell(source) > mainlen) {
        fseek(source,0-(mainlen+1024*64),SEEK_CUR);
        nextchunc();
        mainpos = 1024*64-1;
        return 0;
    }
    return 1;
}

void uprate() {
    if (rate < 9)
        rate++;
    espeak_SetParameter(espeakRATE,rate*ratemult,0);
}

void writesets() {
    char spath[100];
    spath[0] = '\0';
    char *val = getenv("HOME");
    strcat(spath,val);
    strcat(spath,"/.ipagerc");
    FILE *f = fopen(spath,"w");
    fprintf(f,"speech %s %d %s\n", evoice,rate,epunct);
}

void readsets() {
    char spath[100];
    spath[0] = '\0';
    char *val = getenv("HOME");
    strcat(spath,val);
    strcat(spath,"/.ipagerc");
    FILE *f = fopen(spath,"r");
    if (!f) {
        return;
    }
    char sv[40];
    int sr = 0;
    char sp[40];
    char wp[100];
    int k = fscanf(f,"speech %s %d %s\n", sv,&sr,sp);
    if (k == EOF) beep();
    espeak_SetVoiceByName(sv);
    espeak_SetParameter(espeakRATE,ratemult*(rate =sr),0);
    mbstowcs(wp,sp,40);
    espeak_SetPunctuationList(wp);
    strcpy(evoice,sv);
    strcpy(epunct,sp);
    espeak_SetParameter(espeakPUNCTUATION,espeakPUNCT_SOME,0);
    fclose(f);
}

void downrate() {
    if (rate > 1)
        rate--;
    espeak_SetParameter(espeakRATE,rate*ratemult,0);
}

void choosepunct() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"Enter punctuation list: ");
    refresh();
    char p[50];
    char wp[110];
    echo();
    mvgetnstr(getmaxy(stdscr)-1,getcurx(stdscr),p,50);
    noecho();
    strcpy(epunct,p);
    mbstowcs(wp,p,50);
    espeak_SetPunctuationList(wp);
    espeak_SetParameter(espeakPUNCTUATION,espeakPUNCT_SOME,0);
}

void choosevoice() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"Enter voice name: ");
    refresh();
    char v[20];
    echo();
    mvgetnstr(getmaxy(stdscr)-1,getcurx(stdscr),v,20);
    noecho();
    strcpy(evoice,v);
    espeak_SetVoiceByName(v);
}

void readmarks(FILE * mf) {
    int i;
    for (i = 0; i < 27; i++)
        fscanf(mf,"%d\n",&marks[i]);
}

void createmarks() {
    int i;
    for (i = 0; i < 27; i++)
        marks[i] = 0;
}

void savemarks() {
    FILE * mf = fopen(markname,"w");
    int i;
    for (i = 0; i < 26; i++)
        fprintf(mf,"%d\n",marks[i]);
    fprintf(mf,"%d\n",linenum);
    fclose(mf);
}

int gotomark(int n) {
    if (n < 0 || n > 26)
        return 1;
    if (marks[n] > 0) {
        gotoline(marks[n]);
        return 0;
    }
    else
        return 2;
}

void gotomarkchar() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"mark: ");
    refresh();
    char m = getch();
    if (m < 'a' || m > 'z' || marks[m-97] == 0) {
        mvaddstr(getmaxy(stdscr)-1,0,"Invalet mark! Press any key. ");
        refresh();
        getch();
        return;
    }
    gotomark(m-97);
}

void savemark() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"mark: ");
    refresh();
    char m = getch();
    if (m < 'a' || m > 'z') {
        mvaddstr(getmaxy(stdscr)-1,0,"Invalet mark! Press any key. ");
        refresh();
        getch();
        return;
    }
    marks[m-97] = linenum;
}

void initmarks() {
    strcpy(markname,filename);
    strcat(markname,".ipm");
    FILE * mf = fopen(markname,"r");
    if (!mf)
        createmarks();
    else {
        readmarks(mf);
        gotomark(26);
        fclose(mf);
    }
}

void gotobegin() {
    gotopos(0);
    linenum = 1;
}

void gotoend() {
    char data [200];
    int err = 0;
    while (err == 0)
        err = nextline(data);
}

void gotolinenum() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"Enter line number: ");
    refresh();
    int num;
    char snum[5];
    echo();
    mvgetnstr(getmaxy(stdscr)-1,getcurx(stdscr),snum,5);
    noecho();
    num = atoi(snum);
    int err = gotoline(num);
    if (err > 1) {
        mvaddstr(getmaxy(stdscr)-1,0, "Invalet line number! Press any key. ");
        refresh();
        getch();
    }
}

int gotoline(int n) {
    int l = linenum;
    char data[200];
    if (n < 1)
        return 1;
    if (n < l)
        for (; n<l; n++)
            prevline(data);
    else {
        int err = 0;
        for (; n>l&&err==0; n--)
            err = nextline(data);
        if (err>0) {
            gotoline(l);
            return 2;
        }
    }
    return 0;
}

void searchp() {
    int l = linenum;
    refresh();
    char data [getmaxx(stdscr)];
    int err = 0;
    prevline(data);
    strcpy(data,"");
    while (strcasestr(data,sbuf) == NULL&& err == 0)
        err = prevline(data);
    if (err >0) {
        gotoline(l);
        mvaddstr(getmaxy(stdscr)-1,0,"Text not found! Press any key. ");
        refresh();
        getch();
    }
}

void searchf() {
    move(getmaxy(stdscr)-1,0);
    clrtoeol();
    mvaddstr(getmaxy(stdscr)-1,0,"Enter text to find: ");
    refresh();
    echo();
    mvgetnstr(getmaxy(stdscr)-1,getcurx(stdscr),sbuf,100);
    noecho();
    int l = linenum;
    refresh();
    char data [getmaxx(stdscr)];
    int err = 0;
    nextline(data);
    strcpy(data,"");
    while (strcasestr(data,sbuf) == NULL&& err == 0)
        err = nextline(data);
    prevline(data);
    if (err >0) {
        gotoline(l);
        mvaddstr(getmaxy(stdscr)-1,0,"Text not found! Press any key. ");
        refresh();
        getch();
    }
}

void searchn() {
    int l = linenum;
    refresh();
    char data [getmaxx(stdscr)];
    int err = 0;
    nextline(data);
    strcpy(data,"");
    while (strcasestr(data,sbuf) == NULL&& err == 0)
        err = nextline(data);
    prevline(data);
    if (err >0) {
        gotoline(l);
        mvaddstr(getmaxy(stdscr)-1,0,"Text not found! Press any key. ");
        refresh();
        getch();
    }
}


void speakcall(short * wav, int size, espeak_EVENT * evt) {
    int i = 0;
    while (evt[i].type !=0) {
        if (evt[i].type == espeakEVENT_END)
            chars = evt[i].text_position;
        else if (evt[i].type == espeakEVENT_MSG_TERMINATED && sayall) {
            speak();
            break;
        }
        i++;
    }
}

char cchar() {
    return mainbuf[mainpos];
}

int nchar() {
    if (mainpos < mainlen-1)
        mainpos++;
    else if (nextchunc() != 0) {
        return 1;
    }
    datapos++;
    return 0;
}

int pchar() {
    if (datapos == 0)
        return 1;
    if (mainpos > 0)
        mainpos--;
    else if (prevchunc() != 0)
        return 1;
    datapos--;
    return 0;
}

int gotopos(int pos) {
    if (pos > datapos)
        while (pos>datapos)
            nchar();
    else
        while (pos < datapos) {
            pchar();
        }
    return 0;
}

int nextline(char * data) {
    int i = 0;
    char c = 0;
    while (c !='\n') {
        c = cchar();
        data[i] = c;
        i++;
        if (nchar()) {
            return 1;
        }
    }
    data[i] = 0;
    if (strlen(data) == 0)
        return 1;
    linenum++;
//char tmp[140];
//sprintf(tmp,"espeak %s",data);
// system(tmp);
// system("beep");
    return 0;
}

void speak() {
    if (lastline == linenum)
        return;
    lastline = linenum;
    char paragraph[1200];
    char retval[140];
    offset = datapos;
    strcpy(paragraph,"");
    strcpy(retval,"");
    while (strlen(paragraph) < 1000 && strcmp(retval,"\n") != 0) {
        nextline(retval);
        strcat(paragraph,retval);
    }
    espeak_Synth(paragraph,1140,0,POS_CHARACTER,0,1,NULL,NULL);
}

void start_sayall() {
    if (sayall == 0)
        sayall = 1;
    else
        return;
    /*
    pthread_attr_t pa;
    pthread_attr_init(&pa);
    pthread_t thread;
    pthread_create(&thread,&pa,speak,0);
    */
    speak();
}

void stop_sayall() {
    sayall = 0;
    espeak_Cancel();
    lastline = 0;
    int i = offset+chars;
    char data[100];
    while (datapos > i)
        prevline(data);
}

char prevchar(FILE * source) {
    fseek(source,-2, SEEK_CUR);
    return (char) getc(source);
}

int prevline(char * data) {
    if (linenum == 1)
        return 1;
    int i = 0;
    int j = 0;
    if (linenum > 2) {
        while (i < 2) {
            pchar();
            if (cchar()=='\n')
                i++;
            if (i < 2) {
                data[j] = cchar();
                j++;
            }
        }
        data[j] = '\0';
        nchar();
        for (i = 0, j = strlen(data)-1; i<j; i++,j--) {
            char tmp = data[i];
            data[i] = data[j];
            data[j]=tmp;
        }
    }
    else {
        gotopos(0);
        nextline(data);
        linenum -=1;
        gotopos(0);
    }
    linenum-=1;
    return 0;
}

int readpage(char * data) {
    int i = 0;
    char line[200];
    int pos = datapos;
    int num = linenum;
    while (i < getmaxy(stdscr)-1 && nextline(line)==0) {
        strcat(data,line);
        i++;
    }
    linenum = num;
    gotopos(pos);
    return 0;
}

int nextpage() {
    char data[200];
    int i = 0;
    int err = 0;
    while (i < getmaxy(stdscr)-1&&err==0) {
        err = nextline(data);
        i++;
    }
    return 0;
}

int prevpage() {
    char data[200];
    int i = getmaxy(stdscr)-1;
    while (i > 0) {
        prevline(data);
        i--;
    }
    return 0;
}

void printpage() {
    char data[140*79] = "";
    readpage(data);
    clear();
    addstr(data);
    char tmpstr[140];
    sprintf(tmpstr,"%s line %d filepos %d (%d)", filename,linenum,ftell(source),mainlen);
    mvaddstr(getmaxy(stdscr)-1,0,tmpstr);
    move(0,1);
    refresh();
    if (sayall) {
        stop_sayall();
        start_sayall();
    }
}

void pagefile() {
    nextchunc();
    initscr();
    noecho();
    cbreak();
    keypad(stdscr,true);
    int ch = 0;
    initmarks();
    readsets();
    printpage();
    char data[200];
    while (ch != 'q') {
        ch = getch();
        switch(ch) {
        case 'p':
            choosepunct();
            break;
        case 'v':
            choosevoice();
            break;
        case '1':
            downrate();
            break;
        case '2':
            uprate();
            break;
        case ' ':
        case KEY_NPAGE:
            nextpage();
            printpage();
            break;
        case KEY_HOME:
        case 'B':
            gotobegin();
            printpage();
            break;
        case KEY_END:
        case 'E':
            gotoend();
            printpage();
            break;
        case '<':
            searchp();
            printpage();
            break;
        case '>':
            searchn();
            printpage();
            break;
        case 'g':
            gotolinenum();
            printpage();
            break;
        case 'b':
        case KEY_PPAGE:
            prevpage();
            printpage();
            break;
        case KEY_DOWN:
            nextline(data);
            printpage();
            break;
        case KEY_UP:
            prevline(data);
            printpage();
            break;
        case 'a':
            start_sayall();
            break;
        case '/':
            searchf();
            printpage();
            break;
        case 'm':
            gotomarkchar();
            printpage();
            break;
        case 's':
            savemark();
            printpage();
            break;
        case 'c':
            stop_sayall();
            printpage();
            break;
        }
    }
    savemarks();
    writesets();
    endwin();
}

main(int argc, char * argv[]) {
    if (argc == 1) {
        fprintf(stderr, "Usage: ipage <filename>\n");
        return 1;
    }
    char cmd[200];
    strcpy(filename,argv[1]);
    sprintf(cmd,"fold -s \"%s\" | dos2unix -U > /tmp/ipage.tmp",filename);
    int err = system(cmd);
    system("echo >> /tmp/ipage.tmp");
    source = fopen("/tmp/ipage.tmp", "r");
    if (!source|| err) {
        fprintf(stderr,"File doesn't exist!\n");
        return 2;
    }
    espeak_Initialize(AUDIO_OUTPUT_PLAYBACK,100,NULL,1);
    rate = 5;
    espeak_SetParameter(espeakRATE,rate*ratemult,0);
espeak_SetParameter(espeakPITCH,40,0);
espeak_SetParameter(espeakVOLUME,100,0);
    espeak_SetVoiceByName("en-us");
    espeak_SetSynthCallback(&speakcall);
    pagefile();
    espeak_Terminate();
    fclose(source);
    system("rm /tmp/ipage.tmp");
    return 0;
}
